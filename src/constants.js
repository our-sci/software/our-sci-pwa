export const GROUP_PATH_DELIMITER = '/';
export const SPEC_VERSION_SURVEY = 4;
export const SPEC_VERSION_SUBMISSION = 3;
export const SPEC_VERSION_SCRIPT = 2;
export const SPEC_VERSION_GROUP = 2;
