import moment from 'moment';
import * as utils from '@/utils/sandboxUtils';

export default {
  Date,
  JSON,
  Number,
  moment,
  utils,
};
